// Config File
const config = require('../priv/config.json')

module.exports = {
  func: async (msg, args, Strive, logger) => {
    try {
      var owner = await Strive.getRESTUser(config.ownerID)
      msg.channel.createMessage({
        embed: {
          // title: `About ME!`,
          description: `Strive is a bot that is *striving*  to be the best. \nOwner: ${
            owner.username
          }#${owner.discriminator}`,
          author: {
            name: Strive.user.username,
            icon_url: Strive.user.avatarURL
          },
          color: 0x36393e,
          footer: {
            text: `Requested by ${msg.author.username}#${
              msg.author.discriminator
            }`
          }
        }
      })
    } catch (err) {
      logger.error(err)
    }
  },
  name: 'about',
  help: {
    description: 'Information about the bot',
    usage: `${config.prefix}about`
  }
}
