// Config File
const config = require('../priv/config.json')

module.exports = {
  func: async (msg, args, Strive, logger) => {
    var editConfig = (name, value) => {
      Strive.db
        .table('guilds')
        .get(msg.channel.guild.id)
        .run(Strive.db.connection, (err, res) => {
          if (err) throw logger.error(err)
          var config = res.config
          if (value === 'reset') value = null
          config[name] = value
          Strive.db
            .table('guilds')
            .get(res.gid)
            .update({ config: config })
            .run(Strive.db.connection, (err, res) => {
              if (err) throw logger.error(err)
            })
        })
    }
    try {
      switch (args[0]) {
        case 'reset':
          Strive.db
            .table('guilds')
            .get(msg.channel.guild.id)
            .update({
              config: {
                log_channel: null,
                mod_role: null,
                mute_role: null
              }
            })
            .run(Strive.db.connection, (err, res) => {
              if (err) throw logger.error(err)
              msg.channel.createMessage({
                embed: {
                  title: `Config Reset`,
                  author: {
                    name: Strive.user.username,
                    icon_url: Strive.user.avatarURL
                  },
                  color: 0x36393e,
                  footer: {
                    text: `Requested by ${msg.author.username}#${
                      msg.author.discriminator
                    }`
                  }
                }
              })
            })
          break
        case 'mod_role':
          await editConfig('mod_role', args[1])
          msg.channel.createMessage({
            embed: {
              title: `Config Updated`,
              description: `Mod Role: ${args[1]}`,
              author: {
                name: Strive.user.username,
                icon_url: Strive.user.avatarURL
              },
              color: 0x36393e,
              footer: {
                text: `Requested by ${msg.author.username}#${
                  msg.author.discriminator
                }`
              }
            }
          })
          break
        case 'log_channel':
          await editConfig('log_channel', args[1])
          msg.channel.createMessage({
            embed: {
              title: `Config Updated`,
              description: `Log Channel: ${args[1]}`,
              author: {
                name: Strive.user.username,
                icon_url: Strive.user.avatarURL
              },
              color: 0x36393e,
              footer: {
                text: `Requested by ${msg.author.username}#${
                  msg.author.discriminator
                }`
              }
            }
          })
          break
        default:
          Strive.db
            .table('guilds')
            .get(msg.channel.guild.id)
            .run(Strive.db.connection, (err, res) => {
              if (err) throw logger.error(err)
              msg.channel.createMessage(
                `\`\`\`json\n${JSON.stringify(res.config, null, 2)}\`\`\``
              )
            })
          break
      }
    } catch (err) {
      logger.error(err)
    }
  },
  name: 'config',
  help: {
    description: 'Check if the bot is alive',
    usage: `${config.prefix}config`
  }
}
