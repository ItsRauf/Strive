// Config File
const config = require('../priv/config.json')

module.exports = {
  func: async (msg, args, Strive, logger) => {
    try {
      if (!args[0]) {
        msg.channel.createMessage('Please mention someone to kick.')
      }

      var perm = msg.channel.guild.members
        .get(Strive.user.id)
        .permission.has('kickMembers')
      var userperm = msg.channel.guild.members
        .get(msg.author.id)
        .permission.has('kickMembers')
      var member = msg.channel.guild.members.get(msg.mentions[0].id)
      var guildid = msg.channel.guild.id
      var reason = args[1]

      if (!perm) {
        msg.channel.createMessage(
          'Aurora does not have permission to use this command.'
        )
      } else if (!userperm) {
        return msg.channel.createMessage(
          "Sorry, but you don't have the permissions to use this command. " +
            msg.author.mention
        )
      } else if (!member.id) {
        msg.channel.createMessage('Are you sure this user is in this server?')
      } else if (Strive.user.id === member.id) {
        msg.channel.createMessage("I'm sorry, but I can't kick myself.")
      } else if (msg.author.id === member.id) {
        msg.channel.createMessage("Sorry, but you can't kick yourself.")
      } else if (!member.kickable) {
        msg.channel.createMessage(
          "Sorry, but you can't kick this member as they have permissions which allows them not to be kicked."
        )
      } else if (!reason) {
        Strive.kickGuildMember(guildid, member.id)
        Strive.createMessage(msg.channel.id, {
          embed: {
            description: `Kicked ${msg.mentions[0].username}`,
            author: {
              name: Strive.user.username,
              icon_url: Strive.user.avatarURL
            },
            color: 0xfa91b8,
            footer: {
              text: `Command executed by ${msg.author.username}#${
                msg.author.discriminator
              }`
            }
          }
        })
      } else {
        Strive.kickGuildMember(guildid, member.id, `${reason}`)
        Strive.createMessage(msg.channel.id, {
          embed: {
            description: `Kicked ${
              msg.mentions[0].username
            } with reason **${reason}**`,
            author: {
              name: Strive.user.username,
              icon_url: Strive.user.avatarURL
            },
            color: 0xfa91b8,
            footer: {
              text: `Command executed by ${msg.author.username}#${
                msg.author.discriminator
              }`
            }
          }
        })
      }
      // embed: {
      //   title: `\nPong! :ping_pong: `,
      //   description: `\nBot Ping: ${message.timestamp -
      //     msg.timestamp} ms\nApi Ping: ${shard.lastHeartbeatReceived -
      //     shard.lastHeartbeatSent}ms`,
      //   author: {
      //     name: Strive.user.username,
      //     icon_url: Strive.user.avatarURL
      //   },
      //   color: 0x36393e,
      //   footer: {
      //     text: `Requested by ${msg.author.username}#${
      //       msg.author.discriminator
      //     }`
      //   }
      // }
    } catch (err) {
      logger.error(err)
    }
  },
  name: 'kick',
  help: {
    description: 'Check if the bot is alive',
    usage: `${config.prefix}ping`
  }
}
