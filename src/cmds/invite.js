// Config File
const config = require('../priv/config.json')

module.exports = {
  func: async (msg, args, Strive, logger) => {
    try {
      msg.channel.createMessage({
        embed: {
          title: `Invites`,
          description: `Here are all of my invites`,
          author: {
            name: Strive.user.username,
            icon_url: Strive.user.avatarURL
          },
          color: 0x36393e,
          fields: [
            {
              name: 'Bot Invite',
              value:
                '[Link](https://discordapp.com/api/oauth2/authorize?client_id=480567560232894484&permissions=268823767&redirect_uri=https%3A%2F%2Fitsrauf.gq%2F&scope=bot)',
              inline: true
            },
            {
              name: 'Support Server Invite',
              value: '[Link](https://discord.gg/JNRnPhV)',
              inline: true
            }
          ],
          footer: {
            text: `Requested by ${msg.author.username}#${
              msg.author.discriminator
            }`
          }
        }
      })
    } catch (err) {
      logger.error(err)
    }
  },
  name: 'invite',
  help: {
    description: 'Check if the bot is alive',
    usage: `${config.prefix}ping`
  }
}
